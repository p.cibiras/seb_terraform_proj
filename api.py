from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    return "I am a Cloud app. Yeeet!!!"


if __name__ == '__main__':
    from gunicorn.app.wsgiapp import WSGIApplication
    gunicorn_app = WSGIApplication()
    gunicorn_app.app_uri = 'app:app'
    gunicorn_app.run()