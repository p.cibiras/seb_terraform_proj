terraform {
  backend "http" {
  }
}

provider "google" {
  project = "solar-maker-384016"
  region  = "europe-north1-a"
}

resource "google_cloud_run_v2_service" "default" {
  name     = "cloudrun-service"
  location = "us-central1"
  ingress = "INGRESS_TRAFFIC_ALL"

  template {
    containers {
      image = "us-docker.pkg.dev/cloudrun/container/hello"
    }
  }
}